package responder

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/ptflp/godecoder"
	"go.uber.org/zap"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type MockErrorResponder struct {
	err string
}

func (m *MockErrorResponder) Encode(w io.Writer, v interface{}) error {
	return errors.New(m.err)
}

func (m *MockErrorResponder) Decode(r io.Reader, val interface{}) error {
	return errors.New(m.err)
}

func NewMockErrorResponder(err string) *MockErrorResponder {
	return &MockErrorResponder{err: err}
}

func TestRespond_ErrorBadRequest(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(godecoder.NewDecoder(), logger)

	err := fmt.Errorf("bad request error")

	recorder := httptest.NewRecorder()

	r.ErrorBadRequest(recorder, err)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedCode := http.StatusBadRequest
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestErrorBadRequest(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(NewMockErrorResponder("Bad"), logger)

	err := fmt.Errorf("bad request error")

	recorder := httptest.NewRecorder()

	r.ErrorBadRequest(recorder, err)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedCode := http.StatusBadRequest
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestRespond_ErrorForbidden(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(godecoder.NewDecoder(), logger)

	err := fmt.Errorf("forbiden error")

	recorder := httptest.NewRecorder()

	r.ErrorForbidden(recorder, err)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedCode := http.StatusForbidden
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestErrorForbidden(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(NewMockErrorResponder("Forbidden"), logger)

	err := fmt.Errorf("forbiden error")

	recorder := httptest.NewRecorder()

	r.ErrorForbidden(recorder, err)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedCode := http.StatusForbidden
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestRespond_ErrorInternal(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(godecoder.NewDecoder(), logger)

	err := fmt.Errorf("internal error")

	recorder := httptest.NewRecorder()

	r.ErrorInternal(recorder, err)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedCode := http.StatusInternalServerError
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestErrorInternal(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(NewMockErrorResponder("context canceled"), logger)

	err := fmt.Errorf("internal error")

	recorder := httptest.NewRecorder()

	r.ErrorInternal(recorder, err)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedCode := http.StatusInternalServerError
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestErrorInternalContext(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(NewMockErrorResponder("context canceled"), logger)

	recorder := httptest.NewRecorder()

	r.ErrorInternal(recorder, context.Canceled)

	expectedCode := http.StatusOK
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestRespond_ErrorUnauthorized(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(godecoder.NewDecoder(), logger)

	err := fmt.Errorf("unauthorized error")

	recorder := httptest.NewRecorder()

	r.ErrorUnauthorized(recorder, err)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedCode := http.StatusUnauthorized
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestErrorUnauthorized(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(&MockErrorResponder{}, logger)

	err := fmt.Errorf("unauthorized error")

	recorder := httptest.NewRecorder()

	r.ErrorUnauthorized(recorder, err)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedCode := http.StatusUnauthorized
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestRespond_OutputJSON(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(godecoder.NewDecoder(), logger)

	responseData := map[string]string{"message": "test"}

	recorder := httptest.NewRecorder()

	r.OutputJSON(recorder, responseData)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	expectedBody, _ := json.Marshal(responseData)
	actualBody := strings.TrimSuffix(strings.TrimSpace(recorder.Body.String()), "\n")

	if actualBody != string(expectedBody) {
		t.Errorf("Expected body '%s' but got '%s'", expectedBody, actualBody)
	}

	expectedCode := http.StatusOK
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}

func TestOutputJSON(t *testing.T) {
	logger, _ := zap.NewDevelopment()
	//nolint:errcheck
	defer logger.Sync()

	r := NewResponder(&MockErrorResponder{}, logger)

	recorder := httptest.NewRecorder()

	r.OutputJSON(recorder, nil)

	header := recorder.Result().Header["Content-Type"]
	if len(header) == 0 || header[0] != "application/json;charset=utf-8" {
		t.Errorf("Expected 'application/json;charset=utf-8' but got '%s'", header)
	}

	actualBody := strings.TrimSuffix(strings.TrimSpace(recorder.Body.String()), "\n")

	if actualBody != "" {
		t.Errorf("Expected body '%s' but got '%s'", "", actualBody)
	}

	expectedCode := http.StatusOK
	if recorder.Code != expectedCode {
		t.Errorf("Expected code '%d' but got '%d'", expectedCode, recorder.Code)
	}
}
