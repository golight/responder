package examples

import (
	"errors"
	"fmt"
	"github.com/ptflp/godecoder"
	"gitlab.com/golight/responder"
	"go.uber.org/zap"
	"net/http/httptest"
)

func forbidden() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	decoder := godecoder.NewDecoder()

	r := responder.NewResponder(decoder, logger)

	recorder := httptest.NewRecorder()

	r.ErrorForbidden(recorder, errors.New("forbidden"))

	fmt.Println(recorder.Body.String())
	fmt.Println(recorder.Code)
}
