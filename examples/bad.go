package examples

import (
	"errors"
	"fmt"
	"github.com/ptflp/godecoder"
	"gitlab.com/golight/responder"
	"go.uber.org/zap"
	"net/http/httptest"
)

func bad() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	decoder := godecoder.NewDecoder()

	r := responder.NewResponder(decoder, logger)

	recorder := httptest.NewRecorder()

	r.ErrorBadRequest(recorder, errors.New("bad request"))

	fmt.Println(recorder.Body.String())
	fmt.Println(recorder.Code)
}
