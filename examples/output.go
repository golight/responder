package examples

import (
	"fmt"
	"github.com/ptflp/godecoder"
	"gitlab.com/golight/responder"
	"go.uber.org/zap"
	"net/http/httptest"
)

func output() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	decoder := godecoder.NewDecoder()

	r := responder.NewResponder(decoder, logger)

	responseData := map[string]string{"example": "example"}

	recorder := httptest.NewRecorder()

	r.OutputJSON(recorder, responseData)

	fmt.Println(recorder.Body.String())
	fmt.Println(recorder.Code)
}
