package examples

import (
	"errors"
	"fmt"
	"github.com/ptflp/godecoder"
	"gitlab.com/golight/responder"
	"go.uber.org/zap"
	"net/http/httptest"
)

func unauth() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	decoder := godecoder.NewDecoder()

	r := responder.NewResponder(decoder, logger)

	recorder := httptest.NewRecorder()

	r.ErrorUnauthorized(recorder, errors.New("unauthorized"))

	fmt.Println(recorder.Body.String())
	fmt.Println(recorder.Code)
}
