# Responder

Responder of Golite ecosystem.

## Test coverage
- responder.go - 100%

## Installation
```shell
go get "gitlab.com/golight/responder"
```
## Available functions and methods

### New Responder
```go
//A function that creates a new responder instance with the given parameters. She takes over the decoder and recorder.
func NewResponder(decoder godecoder.Decoder, logger *zap.Logger)
```

### Methods
```go
// Sets the "Content-Type" header to "application/json;charset=utf-8" and encodes the data into JSON format.
func (r *Respond) OutputJSON(w http.ResponseWriter, responseData interface{})
```

```go
// Logs an error at the info level, sets the header to "Content Type" to "application/json;charset=utf-8" 
// and sends error code 400 BadRequest with error message encodes into JSON format.
func (r *Respond) ErrorBadRequest(w http.ResponseWriter, err error) 
```

```go
// Logs an error at the warn level, sets the header to "Content Type" to "application/json;charset=utf-8" 
// and sends error code 403 StatusForbidden with error message encodes into JSON format.
func (r *Respond) ErrorForbidden(w http.ResponseWriter, err error)
```

```go
// Logs an error at the warn level, sets the header to "Content Type" to "application/json;charset=utf-8" 
// and sends error code 401 StatusUnauthorized with error message encodes into JSON format.
func (r *Respond) ErrorUnauthorized(w http.ResponseWriter, err error)
```

```go
// Checks if the error is due to a context being canceled. If so, it returns early. 
// Else logs an error at the error level, sets the header to "Content Type" to "application/json;charset=utf-8" 
// and sends error code 500 StatusInternalServerError with error message encodes into JSON format.
func (r *Respond) ErrorInternal(w http.ResponseWriter, err error)
```
## Usage example
### More are in the examples folder

```go
package main

import (
    "gitlab.com/golight/responder"
    "github.com/ptflp/godecoder"
    "go.uber.org/zap"
    "net/http/httptest"
    "fmt"
)

func main() {
    logger, _ := zap.NewProduction()
    defer logger.Sync()

    decoder := godecoder.NewDecoder()

    r := responder.NewResponder(decoder, logger)

    responseData := map[string]string{"example": "example"}

    recorder := httptest.NewRecorder()

    r.OutputJSON(recorder, responseData)

    fmt.Println(recorder.Body.String())
    fmt.Println(recorder.Code)
}


```


