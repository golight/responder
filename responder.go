package responder

import (
	"context"
	"errors"
	"github.com/ptflp/godecoder"
	"gitlab.com/golight/responder/messages"
	"go.uber.org/zap"
	"net/http"
)

// Responder is an interface that defines methods for sending HTTP responses with various status codes.
type Responder interface {
	OutputJSON(w http.ResponseWriter, responseData interface{})
	ErrorUnauthorized(w http.ResponseWriter, err error)
	ErrorBadRequest(w http.ResponseWriter, err error)
	ErrorForbidden(w http.ResponseWriter, err error)
	ErrorInternal(w http.ResponseWriter, err error)
}

// Respond implements the Responder interface using a provided logger and encoder.
type Respond struct {
	log *zap.Logger
	godecoder.Decoder
}

// NewResponder receives decoder and logger, returns responder.
func NewResponder(decoder godecoder.Decoder, logger *zap.Logger) Responder {
	return &Respond{log: logger, Decoder: decoder}
}

// OutputJSON sets the "Content-Type" header to "application/json;charset=utf-8" and encodes the data into JSON format.
func (r *Respond) OutputJSON(w http.ResponseWriter, responseData interface{}) {
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := r.Encode(w, responseData); err != nil {
		r.log.Error("responder json encode error", zap.Error(err))
	}
}

// ErrorBadRequest logs an error at the info level, sets the header to "Content Type" to "application/json;charset=utf-8" and sends error code 400 BadRequest with error message encodes into JSON format.
func (r *Respond) ErrorBadRequest(w http.ResponseWriter, err error) {
	r.log.Info("http response bad request status code", zap.Error(err))
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusBadRequest)
	if err := r.Encode(w, messages.Response{
		Success: false,
		Message: err.Error(),
		Data:    nil,
	}); err != nil {
		r.log.Info("response writer error on write", zap.Error(err))
	}
}

// ErrorForbidden logs an error at the warn level, sets the header to "Content Type" to "application/json;charset=utf-8" and sends error code 403 StatusForbidden with error message encodes into JSON format.
func (r *Respond) ErrorForbidden(w http.ResponseWriter, err error) {
	r.log.Warn("http resposne forbidden", zap.Error(err))
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusForbidden)
	if err := r.Encode(w, messages.Response{
		Success: false,
		Message: err.Error(),
		Data:    nil,
	}); err != nil {
		r.log.Error("response writer error on write", zap.Error(err))
	}
}

// ErrorUnauthorized logs an error at the warn level, sets the header to "Content Type" to "application/json;charset=utf-8" and sends error code 401 StatusUnauthorized with error message encodes into JSON format.
func (r *Respond) ErrorUnauthorized(w http.ResponseWriter, err error) {
	r.log.Warn("http resposne Unauthorized", zap.Error(err))
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusUnauthorized)
	if err := r.Encode(w, messages.Response{
		Success: false,
		Message: err.Error(),
		Data:    nil,
	}); err != nil {
		r.log.Error("response writer error on write", zap.Error(err))
	}
}

// ErrorInternal checks if the error is due to a context being canceled. If so, it returns early. Else logs an error at the error level, sets the header to "Content Type" to "application/json;charset=utf-8" and sends error code 500 StatusInternalServerError with error message encodes into JSON format.
func (r *Respond) ErrorInternal(w http.ResponseWriter, err error) {
	if errors.Is(err, context.Canceled) {
		return
	}
	r.log.Error("http response internal error", zap.Error(err))
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusInternalServerError)
	if err := r.Encode(w, messages.Response{
		Success: false,
		Message: err.Error(),
		Data:    nil,
	}); err != nil {
		r.log.Error("response writer error on write", zap.Error(err))
	}
}
